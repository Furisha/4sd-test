// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import 'cypress-file-upload';
import 'cypress-wait-until';
import 'cy-mobile-commands';
import 'cypress-iframe'

// open homepage on stage environment
Cypress.Commands.add('openHomePage', () => {
    cy.visit(Cypress.env('stageUrl'))
})
// open homepage on stage environment and load users data
Cypress.Commands.add('openHomePageAndLoadUsersData', () => {
    cy.visit(Cypress.env('stageUrl'))
    cy.fixture('users').then(function (users) {
        this.users = users
    })
})
// open homepage on stage environment on ipad-2 and load users data
Cypress.Commands.add('openHomePageiPad2', () => {
    cy.viewport('ipad-2')
    cy.visit(Cypress.env('stageUrl'))
    cy.fixture('users').then(function (users) {
        this.users = users
    })    
})
// using this workaround for 3rd party webs... Srtipe...
Cypress.Commands.add('getIframe', (iframe) => {
    return cy.get(iframe)
        .its('0.contentDocument.body')
        .should('be.visible')
        .then(cy.wrap);
})


