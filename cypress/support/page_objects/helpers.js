export class HelpFunc {

    deleteAllPhotos() {

        cy.get('.css-1jlpf7p').find('.css-tkhjpi').then(listing => {
            const photoCounter = Cypress.$(listing).length
            expect(listing).to.have.length(photoCounter)

            for (let i = 0; i < photoCounter; i++) {
                cy.intercept('DELETE', '**/photo/*').as('deletePhoto') // Intercept DELETE method
                cy.get('.css-1jlpf7p').find('span').last().should('contain.text', 'Delete').click({ force: true }).wait(1000).then(el => {
                    cy.get('body').find('.css-rqxzq7').click().wait(1000)
                    cy.waitUntil(() => cy.get('[role="alert"]').click())
                });
                cy.wait('@deletePhoto').its('response.statusCode').should('be.oneOf', [200]) // Verify Status code
            }
        })
    }

    deleteAllVideos() {

        cy.get('.css-1jlpf7p').find('.css-tkhjpi').then(listing => {
            const videoCounter = Cypress.$(listing).length
            expect(listing).to.have.length(videoCounter)

            for (let i = 0; i < videoCounter; i++) {
                cy.intercept('DELETE', '**/video/*').as('deleteVideo') // Intercept DELETE method
                cy.get('.css-1jlpf7p').find('span').last().should('contain.text', 'Delete').click({ force: true }).wait(1000).then(el => {
                    cy.get('body').find('.css-rqxzq7').click().wait(1000)
                    cy.waitUntil(() => cy.get('[role="alert"]').click())
                });
                cy.wait('@deleteVideo').its('response.statusCode').should('be.oneOf', [200]) // Verify Status code
            }
        })
    }

    selectCurvyCheckBox() {

        cy.get('.css-mnm23').find('label').contains('Curvy').find('[value="5"]').as('CurvyCheckBox')

        cy.get('@CurvyCheckBox').parent().then($body => {
            if ($body.hasClass('checked')) {
                cy.log('Already checked')
            } else {
                cy.intercept('GET', '**/search*').as('getSearch')
                cy.get('@CurvyCheckBox').check({ force: true })
                cy.log('Checked').wait(1000)
                cy.wait('@getSearch').then(xhr => {
                    console.log(xhr)
                    expect(xhr.response.statusCode).to.equal(200)
                    expect(xhr.state).to.equal('Complete')
                })
            }
        })
    }

    selectFullFiguredCheckBox() {

        cy.get('.css-mnm23').find('label').contains('Full figured').find('[value="6"]').as('FullfiguredCheckBox')

        cy.get('@FullfiguredCheckBox').parent().then($body => {
            if ($body.hasClass('checked')) {
                cy.log('Already checked')
            } else {
                cy.intercept('GET', '**/search*').as('getSearch')
                cy.get('@FullfiguredCheckBox').check({ force: true })
                cy.log('Checked').wait(1000)
                cy.wait('@getSearch').then(xhr => {
                    console.log(xhr)
                    expect(xhr.response.statusCode).to.equal(200)
                    expect(xhr.state).to.equal('Complete')
                })
            }
        })
    }

    mobileSizes(size) {
        if (Cypress._.isArray(size)) {
            cy.viewport(size[0], size[1])
        } else {
            cy.viewport(size)
        }
    }

    postAddress() {

        cy.fixture('request').then(function (request) {
            this.request = request

            cy.request('POST', 'https://stage.4sd.com/api/auth/login', request.userCredentials_HefnerJacket)
            .its('body').then(body => {
                const token = body.access_token
                cy.request({
                    url: 'https://stage.4sd.com/api/user',
                    headers: { 'Authorization': 'Bearer ' + token },
                    method: 'POST',
                    body: request.bodyRequestChangeAddress
                })
            })
        })  

    }

    postAddressDefault() {

        cy.fixture('request').then(function (request) {
            this.request = request

            cy.request('POST', 'https://stage.4sd.com/api/auth/login', request.userCredentials_HefnerJacket)
            .its('body').then(body => {
                const token = body.access_token
                cy.request({
                    url: 'https://stage.4sd.com/api/user',
                    headers: { 'Authorization': 'Bearer ' + token },
                    method: 'POST',
                    body: request.bodyRequestBackAddress
                })
            })
        })
            
    }

    getEmailLink(body) {

        var parser = new DOMParser();
        var doc = parser.parseFromString(body, 'text/html');

        var element = doc.body.getElementsByTagName('a');

        for (var i = 0; i < element.length; i++) {
            if (element[i].href.includes('verifyLink')) {
                return element[i].href;
            }
        }

        return false;

    }
}

export const onHelpFunc = new HelpFunc()