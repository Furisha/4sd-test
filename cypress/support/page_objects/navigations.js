export class Navigations {

    navigateToLogin() {
        cy.waitUntil(() => cy.get('header').find('.css-1ffs3f2').contains('Login').click())
    }

    navigateToSubmit() {
        cy.waitUntil(() => cy.get('button[type=submit]').click())
    }

    navigateToSearch() {
        cy.intercept('GET', '**/search*').as('getSearch') // Intercept GET
        cy.get('header').contains('Search').click()
        cy.url().should('include', '/search') // URL Verification
        cy.wait('@getSearch').its('response.statusCode')
            .should('be.oneOf', [200]) // Verify Status code
    }

    navigateToMessages() {
        cy.intercept('GET', '**/conversation*').as('getMessage')
        cy.get('header').contains('Messages').click()
        cy.url().should('include', '/messages')
        cy.wait('@getMessage').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToMatches() {
        cy.intercept('GET', '**/admirers*').as('getAdmirers')
        cy.get('header').contains('Matches').click()
        cy.url().should('include', '/matches')

        cy.wait('@getAdmirers').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToAccess() {
        cy.intercept('GET', '**/access*').as('getAccess')
        cy.get('header').contains('Access').click()
        cy.url().should('include', '/access')
        cy.wait('@getAccess').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToProfile() {
        cy.intercept('GET', '**/user*').as('getUser')
        cy.get('#root').find('header').contains('Profile').click();

        cy.wait('@getUser').its('response.statusCode')
            .should('be.oneOf', [200])

        cy.get('body').find('#modal-root').then($body => {
            if ($body.find('#buyCreditsFromInitialModal').length > 0) {
                cy.get('#modal-root').find('#buyCreditsFromInitialModal').click()
                cy.log(`************** Close Modal`)
            }
        })

        cy.url().should('include', '/profile/edit-profile')
        cy.log(` **************************** Suggar Daddy Automation Ranger is Back !!!!! :) `)
    }

    navigateToFemaleProfile() {
        cy.intercept('GET', '**/user*').as('getUser')
        cy.get('#root').find('header').contains('Profile').click();

        cy.wait('@getUser').its('response.statusCode')
            .should('be.oneOf', [200])

        cy.get('body').find('#modal-root').then($body => {
            if ($body.find('.css-g0f2cg').length > 0) {
                cy.get('.css-1b1n5w').find('.css-g0f2cg').click()
                cy.log(`************** Close Modal`)
            }
        })

        cy.url().should('include', '/profile/edit-profile')
        cy.log(` **************************** Suggar Daddy Automation Ranger is Back !!!!! :) `)
    }

    navigateToProfileMenu() {
        cy.get('.css-1pzi275').find('[class=" dating dating-chevron-down css-31ybf8 e1ujef100"]').click()
    }

    navigateToBurgerMenu() {
        cy.waitUntil(() => cy.get('.css-5axqcn').as('burgerMenu'))
        cy.get('@burgerMenu').click()
    }

    navigateToBuy100Credits() {

        cy.get('.css-rkptj6').next().find('button').should('contain.text', 'BUY CREDITS').click()

        cy.intercept('POST', '**/twispay').as('buyCredits')
        cy.get('#buyCreditsPlan3Btn').should('be.visible').click()
        cy.wait('@buyCredits').then(xhr => {
            console.log(xhr)
            expect(xhr.response.statusCode).to.equal(200)
        })
    }

    navigateToLogout() {
        cy.intercept('POST', '**/logout*').as('postLogout')
        cy.get('.css-1xo8pcw').contains('Logout').click({ force: true })
        cy.wait('@postLogout').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToProfileTab() {
        cy.get('nav').find('a[href="/profile/edit-profile"]').click()
    }

    navigateToViewPage() {
        cy.waitUntil(() => cy.get('.nameBlock').find('.css-12azz1w').should('contain.text', 'VIEW PROFILE').click())
    }

    navigateToPhotosTab() {
        cy.intercept('GET', '**/photo*').as('getPhoto')
        cy.get('nav').find('a[href="/profile/photos/public"]').click()
        cy.wait('@getPhoto').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToVideosTab() {
        cy.intercept('GET', '**/video*').as('getVideo')
        cy.get('nav').find('a[href="/profile/videos/public"]').click()
        cy.wait('@getVideo').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToVerificationTab() {
        cy.get('nav').find('a[href="/profile/verification"]').click()
        cy.get('#modal-root').then($body => {
            if ($body.find('[type="basic"]').length > 0) {
                cy.get('#modal-root').find('.css-rie60e').click()
                cy.log(`************** Close Modal`)
            }
        })
    }

    navigateToAdmirersTab() {
        cy.get('nav').find('a[href="/matches/admirers"]').click()
    }

    navigateToFavoritesTab() {
        cy.intercept('GET', '**/favorite*').as('getFavorite')
        cy.get('nav').find('a[href="/matches/favorites"]').click()
        cy.wait('@getFavorite').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToVisitorsTab() {
        cy.intercept('GET', '**/visitors*').as('getVisitors')
        cy.get('nav').find('a[href="/matches/visitors"]').click()
        cy.wait('@getVisitors').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToViewedTab() {
        cy.intercept('GET', '**/views*').as('getViews')
        cy.get('nav').find('a[href="/matches/views"]').click()
        cy.wait('@getViews').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToPendingTab() {
        cy.intercept('GET', '**/pending*').as('getPending')
        cy.get('nav').find('a[href="/access/pending"]').click()
        cy.wait('@getPending').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToApprovedTab() {
        cy.intercept('GET', '**/approved*').as('getApprove')
        cy.get('nav').find('a[href="/access/approved"]').click()
        cy.wait('@getApprove').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    navigateToDeclinedTab() {
        cy.intercept('GET', '**/declined*').as('getDeclined')
        cy.get('nav').find('a[href="/access/declined"]').click()
        cy.wait('@getDeclined').its('response.statusCode')
            .should('be.oneOf', [200])
    }

    modalStayBasicMobile() {
        cy.get('body').then($body => {
            if ($body.find('#modal-root').length > 0) {
                cy.get('#modal-root').find('.css-rie60e').click();
                cy.log(`I just closed the Campaign`)
            } else {
                cy.log(`Modal not exist`)
            }
        })
    }

    modalStayBasic() {
        cy.wait(1000)
        cy.get('body').find('#modal-root').then($body => {
            if ($body.find('#buyCreditsFromInitialModal').length > 0) {
                cy.get('#modal-root').find('#buyCreditsFromInitialModal').click()
                cy.log(`************** Close Modal`)
            }
        })
    }

}

export const onNavigations = new Navigations()