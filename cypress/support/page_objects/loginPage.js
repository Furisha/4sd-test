export class LoginPage {

    loginUser() {
        // This profile has no Intercept method. Intercept POST method is in TEST for this user
        cy.waitUntil(() => cy.get('#root').should('contain.text', 'WELCOME BACK'))
        cy.fixture('users').then((users) => {
            const emailUser = users.emailUser;
            const passwordUser = users.passwordUser;
            cy.get('[type="email"]').clear().type(emailUser)
            cy.get('input[name=password]').type(passwordUser)
        })
        cy.get('button[type=submit]').click()

        cy.get('[type="basic"]').then($body => {
            if ($body.length > 0) {
                cy.get('#modal-root').find('.css-rie60e').click()
                cy.log(`************** Close Modal`)
            }
        })

        cy.get('header').find('h3 > span').first().then(($header_name) => {
            expect($header_name).to.contain.text('HeUser')
        })
    }

    loginSDRangerUser() {
        cy.intercept('POST', '**/login').as('postLogin')
        cy.fixture('users').then((users) => {
            const emailSDRanger = users.emailSDRanger;
            const passwordSDRanger = users.passwordSDRanger;
            cy.get('input[name=email]').clear().type(emailSDRanger)
            cy.get('input[name=password]').type(passwordSDRanger)
        })

        cy.get('button[type=submit]').click()
        cy.wait('@postLogin').its('response.statusCode')
            .should('be.oneOf', [200])

        cy.get('.css-3t36qx').find('span').first().then(($header_name) => {
            expect($header_name).to.contain.text('SDRanger')
        })
        // If user has no more credits left, buy 100 credits with card
        cy.get('.css-rkptj6').find('span').then($body => {
            const counter = $body.text()
            if ($body.text() == 0) {
                cy.log('I have 0 coins and I will now buy some coins for 100 bucks')
                cy.waitUntil(() => cy.get('.css-17cdmfr > :nth-child(2) > a > #buyCreditsTopMenuBtn').click())

                cy.intercept('POST', '**/twispay').as('buyCredits')
                cy.waitUntil(() => cy.get('#buyCreditsPlan3Btn').click())
                cy.wait('@buyCredits').then(xhr => {
                    console.log(xhr)
                    expect(xhr.response.statusCode).to.equal(200)
                })

                cy.waitUntil(() => cy.get('iframe[name="twispay-form"]').should('be.visible').then(res => {

                    cy.wrap(res.contents())
                        .find('input[id="card-holder"]').type('Test User')

                    cy.wrap(res.contents())
                        .find('input[id="card-number"]').type('4242424242424242')

                    cy.wrap(res.contents())
                        .find('input[id="card-expiry-month"]').type('0223')

                    cy.wrap(res.contents())
                        .find('input[id="cvv-code"]').type('123')

                    cy.intercept('GET', '**/balance').as('getBalance')

                    cy.wrap(res.contents())
                        .find('button').should('contain.text', '59').click()

                    cy.wait('@getBalance').then(xhr => {
                        console.log(xhr)
                        expect(xhr.response.statusCode).to.equal(200)
                        //expect(xhr.response.body.balance).to.equal('*****NEW VALUE*****')
                    })

                    cy.intercept('GET', '**/search').as('getSearch')
                    cy.get('.home-button').should('contain.text', 'BACK HOME').click()
                    cy.wait('@getSearch').then(xhr => {
                        console.log(xhr)
                        expect(xhr.response.statusCode).to.equal(200)
                    })
                }))
            }
        })
    }

    loginSDPopayeUser() {
        cy.fixture('users').then((users) => {
            const emailSDPopaye = users.emailSDPopaye;
            const passwordSDPopaye = users.passwordSDPopaye;
            cy.get('[type="email"]').clear().type(emailSDPopaye)
            cy.get('input[name=password]').type(passwordSDPopaye)
        })
        cy.get('button[type=submit]').click()

        cy.get('[type="basic"]').then($body => {
            if ($body.length > 0) {
                cy.get('#modal-root').find('.css-rie60e').click()
                cy.log(`************** Close Modal`)
            }
        })

        cy.get('header').find('h3 > span').first().then(($header_name) => {
            expect($header_name).to.contain.text('SDPopaye')
        })
    }

    loginShe05User() {
        cy.intercept('POST', '**/login').as('postLogin') // Intercept POST
        cy.waitUntil(() => cy.get('#root').should('contain.text', 'WELCOME BACK'))
        cy.fixture('users').then((users) => {
            const emailShe05 = users.emailShe05;
            const passwordShe05 = users.passwordShe05;
            cy.get('input[name=email]').clear().type(emailShe05)
            cy.get('input[name=password]').type(passwordShe05)
        })
        cy.get('button[type=submit]').click()
        cy.wait('@postLogin').its('response.statusCode')
            .should('be.oneOf', [200]) // Verify Status code

        cy.get("body").then($body => {
            if ($body.find("#modal-root").length > 0) {
                cy.get('.css-1b1n5w').find('.css-g0f2cg').click({ multiple: true });
            }
        })
    }

    loginShe05UserWithoutModal() {
        cy.intercept('POST', '**/login').as('postLogin') // Intercept POST
        cy.waitUntil(() => cy.get('#root').should('contain.text', 'WELCOME BACK'))
        cy.fixture('users').then((users) => {
            const emailShe05 = users.emailShe05;
            const passwordShe05 = users.passwordShe05;
            cy.get('input[name=email]').clear().type(emailShe05)
            cy.get('input[name=password]').type(passwordShe05)
        })
        cy.get('button[type=submit]').click()
        cy.wait('@postLogin').its('response.statusCode')
            .should('be.oneOf', [200]) // Verify Status code
    }

    loginShe04User() {
        cy.intercept('POST', '**/login').as('postLogin') // Intercept POST
        cy.waitUntil(() => cy.get('#root').should('contain.text', 'WELCOME BACK'))
        cy.fixture('users').then((users) => {
            const emailShe04 = users.emailShe04;
            const passwordShe04 = users.passwordShe04;
            cy.get('input[name=email]').clear().type(emailShe04)
            cy.get('input[name=password]').type(passwordShe04)
        })
        cy.get('button[type=submit]').click()
        cy.wait('@postLogin').its('response.statusCode')
            .should('be.oneOf', [200]) // Verify Status code

        cy.get("body").then($body => {
            if ($body.find("#modal-root").length > 0) {
                cy.get('.css-1b1n5w').find('.css-g0f2cg').click({ multiple: true });
            }
        })
    }

    loginShe09User() {
        cy.intercept('POST', '**/login').as('postLogin') // Intercept POST
        cy.waitUntil(() => cy.get('#root').should('contain.text', 'WELCOME BACK'))
        cy.fixture('users').then((users) => {
            const emailShe09 = users.emailShe09;
            const passwordShe09 = users.passwordShe09;
            cy.get('input[name=email]').clear().type(emailShe09)
            cy.get('input[name=password]').type(passwordShe09)
        })
        cy.get('button[type=submit]').click()
        cy.wait('@postLogin').its('response.statusCode')
            .should('be.oneOf', [200]) // Verify Status code

        cy.get("body").then($body => {
            if ($body.find("#modal-root").length > 0) {
                cy.get('.css-1b1n5w').find('.css-g0f2cg').click({ multiple: true });
            }
        })
    }

    loginWithEmailAndPassword(email, password) {
        cy.intercept('POST', '**/login').as('postLogin') // Intercept POST
        cy.contains('#root', 'WELCOME BACK').find('form').then(form => {
            cy.wrap(form).find('[type="email"]').type(email)
            cy.wrap(form).find('[type="password"]').type(password)
            // cy.wrap(form).find('[type="checkbox"]').check({ force: true })
            cy.wrap(form).submit()
        })
        cy.wait('@postLogin').its('response.statusCode')
            .should('be.oneOf', [200]) // Verify Status code
    }

}

export const onLoginPage = new LoginPage()