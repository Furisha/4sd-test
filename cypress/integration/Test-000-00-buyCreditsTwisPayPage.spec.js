import { onNavigations } from "../support/page_objects/navigations";
import { onLoginPage } from "../support/page_objects/loginPage";

describe(`Test-002-04 VERIFY: BUY CREDITS - Valid/Invalid`, () => {

    before('open application', () => {
        cy.openHomePage()
    });

    it('TC-01 VERIFY: TC-01 Login - SugarDaddy - HeUser - Navigate to BUY CREDITS', () => {

        onNavigations.navigateToLogin()
        onLoginPage.loginSDRangerUser()
        onNavigations.navigateToBuy100Credits()
        
    })

    it(`TC-02 VERIFY: Leave Form EMPTY - Submit - VERIFY: Validation messages`, function () {

        cy.iframe().find('button')
            .should('contain.text', '59')
            .should('contain.text', 'Pay')
            .should('have.class', 'btn-primary')
            .click()

        cy.iframe().find('.help-block').should('contain.text', 'Please enter card holder name')
        cy.iframe().find('.help-block').should('contain.text', 'Please enter the card number')
        cy.iframe().find('.help-block').should('contain.text', 'Please choose the expiry date')
        cy.iframe().find('.help-block').should('contain.text', 'Please enter the CVV number')

    });

    it(`TC-03 VERIFY: Leave EMPTY Date and Expire Date Field - Submit - VERIFY: Validation messages`, function () {

        cy.iframe().find('#card-holder').clear().type('users.cardholder$$$')
        cy.iframe().find('input[id="card-number"]').clear().type('4242424242424242')
        cy.iframe().find('button')
            .should('contain.text', '59')
            .should('contain.text', 'Pay')
            .should('have.class', 'btn-danger')
            .click()

        cy.iframe().find('.help-block').should('contain.text', 'Please choose the expiry date')
        cy.iframe().find('.help-block').should('contain.text', 'Please enter the CVV number')

    });

    it(`TC-04 VERIFY: Fill form - Submit - VERIFY: Button is ACTIVE`, function () {

        cy.iframe().find('#card-holder').clear().type('users.cardholder$$$')
        cy.iframe().find('input[id="card-number"]').clear().type('4242424242424242')
        cy.iframe().find('input[id="card-expiry-month"]').type('0223')
        cy.iframe().find('input[id="cvv-code"]').type('123')
        cy.iframe().find('button')
            .should('contain.text', '59')
            .should('contain.text', 'Pay')
            .should('have.class', 'btn-primary')

    });

});
