import { onNavigations } from "../support/page_objects/navigations";
import { onLoginPage } from "../support/page_objects/loginPage";

describe(`Test-002-05 VERIFY: BUY CREDITS`, () => {

    before('open application', () => {
        cy.openHomePage()
    });

    it('TC-01 VERIFY: TC-01 Login - SugarDaddy - HeUser - BUY 100 CREDITS', () => {

        onNavigations.navigateToLogin()
        onLoginPage.loginSDRangerUser()

        cy.get('.css-rkptj6').find('span').should('be.visible').then($body => {

            const counterbeforebuy = $body.text()
            if ($body.text() > 0) {

                onNavigations.navigateToBuy100Credits()

                cy.wait(10000)
                cy.frameLoaded('iframe[name="twispay-form"]')

                cy.iframe().find('#card-holder').clear().type('users.cardholder$$$')
                cy.iframe().find('input[id="card-number"]').clear().type('4242424242424242')
                cy.iframe().find('input[id="card-expiry-month"]').type('0223')
                cy.iframe().find('input[id="cvv-code"]').type('123')

                cy.intercept('GET', '**/balance').as('getBalance')
                cy.iframe().find('button')
                    .should('contain.text', '59')
                    .should('contain.text', 'Pay')
                    .should('have.class', 'btn-primary').click().wait(10000)
                cy.wait('@getBalance').then(xhr => {
                    console.log(xhr)
                    expect(xhr.response.statusCode).to.equal(200)
                })

                cy.intercept('GET', '**/search*').as('getSearch')
                cy.get('.home-button').should('contain.text', 'BACK HOME').click()
                cy.wait('@getSearch').then(xhr => {
                    console.log(xhr)
                    expect(xhr.response.statusCode).to.equal(200)
                })

            }

            cy.get('.css-rkptj6').find('span').should('be.visible').then($body => {
                const counterafterebuy = $body.text()
                expect(counterbeforebuy).not.to.contain(counterafterebuy)
            })

        })

    })

});
